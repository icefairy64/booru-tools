from http_client import HTTPClient
from urllib.parse import urlencode
from json import loads


class DanbooruEntityAPI:
    def __init__(self, root_url: str, entity_type: str):
        self.http = HTTPClient(root_url)
        self.entity_type = entity_type

    def get(self, **kwargs):
        if len(kwargs) == 0:
            raise Exception("Empty kwargs")
        query = {k: v for k, v in kwargs.items() if v is not None}

        entity_type = self.entity_type
        if entity_type == "ugoira":
            entity_type = "media_metadata"

        path = f"/{entity_type}.json?{urlencode(query)}"

        if self.entity_type == "ugoira":
            path += "&search[metadata][File:FileType]=ZIP&only=metadata[Ugoira:FrameDelays],media_asset_id,id"

        body = self.http.get(path)
        json = loads(body)

        if isinstance(json, dict):
            return json

        if isinstance(json, list):
            return json

        raise Exception("Not a dict or list")

    def close(self):
        self.http.close()


def danbooru_api_pages(root_url: str, entity_type: str, start_id: int, limit: int, group_by: str | None = None):
    api = DanbooruEntityAPI(root_url, entity_type)
    try:
        has_more = True
        cur_id = start_id
        while has_more:
            page = api.get(page=f"a{cur_id}", limit=limit, group_by=group_by)
            if not isinstance(page, list):
                raise Exception('Not a list')
            if len(page) == 0:
                return
            cur_id = page[0]['id']
            yield page
    finally:
        api.close()
