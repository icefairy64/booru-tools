from time import sleep
from argparse import ArgumentParser
from storage import create_sqlite_storage, EntityRecord
from api import danbooru_api_pages
from json import dumps, load
from os import scandir


def sync(args: object):
    root_url = args.root_url
    entity_type = args.entity_type
    storage = args.storage
    group_by = args.group_by

    db = create_sqlite_storage(storage, entity_type)

    last_entity = db.last_entity()
    last_id = 0
    if last_entity is not None:
        last_id = last_entity.id

    print(f"Site: {root_url}")
    print(f"Fetching {entity_type} from id {last_id}")

    for page in danbooru_api_pages(root_url, entity_type, last_id, 250, group_by=group_by):
        min_id = page[len(page) - 1]['id']
        max_id = page[0]['id']
        print(f"Fetched range {min_id} - {max_id}")
        records = [EntityRecord(entity['id'], dumps(entity, separators=(",", ":"))) for entity in page]
        db.insert_page(records)
        sleep(0.75)

    db.close()


def ingest(args: object):
    root_dir = args.root_dir
    entity_type = args.entity_type
    storage = args.storage

    db = create_sqlite_storage(storage, entity_type)

    last_entity = db.last_entity()
    last_id = 0
    if last_entity is not None:
        last_id = last_entity.id

    print(f"Directory: {root_dir}")

    def ingest_json(path: str):
        print(f"Ingesting {path}")
        with open(path, mode="r", encoding="utf-8") as file:
            data = load(file)
        if isinstance(data, dict):
            data = data[entity_type]
        records = [EntityRecord(entity['id'], dumps(entity, separators=(",", ":"))) for entity in data]
        db.insert_page(records)
        print(f"Ingested {len(records)} items")

    def ingest_dir(path: str):
        for item in scandir(path):
            if item.is_file() and item.name.lower().endswith(".json"):
                ingest_json(item.path)
            if item.is_dir():
                ingest_dir(item.path)

    ingest_dir(root_dir)

    db.close()


if __name__ == '__main__':
    parser = ArgumentParser()

    parser.add_argument("--storage", required=True, help="SQLite storage path")

    subparsers = parser.add_subparsers(title="subcommands")

    sync_parser = subparsers.add_parser(name="sync",
                                        help="sync from remote server",
                                        description="sync from remote server")
    sync_parser.add_argument("--root_url", required=True)
    sync_parser.add_argument("--entity_type", required=True)
    sync_parser.add_argument("--group_by")
    sync_parser.set_defaults(func=sync)

    ingest_parser = subparsers.add_parser(name="ingest",
                                          help="ingest from local directory",
                                          description="ingest from local directory")
    ingest_parser.add_argument("--root_dir", required=True)
    ingest_parser.add_argument("--entity_type", required=True)
    ingest_parser.set_defaults(func=ingest)

    result = parser.parse_args()
    result.func(result)
