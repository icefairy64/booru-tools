from http.client import HTTPConnection, HTTPSConnection
from urllib.parse import urlparse


class HTTPClient:
    def __init__(self, root_url: str):
        parse_res = urlparse(root_url)
        host = parse_res.netloc
        ctor = HTTPConnection if parse_res.scheme == 'http' else HTTPSConnection
        self.http = ctor(host)

    def get(self, path: str):
        self.http.request(method='GET', url=path, headers={"User-Agent": "BreezyDapiTools/0.1"})
        resp = self.http.getresponse()
        content = resp.read()
        return str(content, encoding='utf-8')

    def close(self):
        self.http.close()
