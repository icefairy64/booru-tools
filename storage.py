import sqlite3


class EntityRecord:
    def __init__(self, entity_id: int, contents: str):
        self.id = entity_id
        self.contents = contents


class SqliteStorage:
    def __init__(self, path: str, table: str):
        self.db = sqlite3.connect(path)
        self.table = table
        self.__migrate()

    def __migrate(self):
        self.__ensure_table()

    def __ensure_table(self):
        cur = self.db.cursor()
        cur.execute(f"CREATE TABLE IF NOT EXISTS {self.table}(id int, contents text)")
        cur.execute(f"CREATE UNIQUE INDEX IF NOT EXISTS {self.table}_id_idx ON {self.table} (id)")
        cur.close()

    def close(self):
        self.db.close()

    def last_entity(self):
        cur = self.db.cursor()
        res = cur.execute(f"SELECT id, contents FROM {self.table} ORDER BY id DESC LIMIT 1")
        match res.fetchone():
            case (entity_id, contents):
                res = EntityRecord(entity_id, contents)
            case _:
                res = None
        cur.close()
        return res

    def insert_page(self, page: list[EntityRecord]):
        cur = self.db.cursor()
        params = [(entity.id, entity.contents) for entity in page]
        cur.executemany(f"INSERT INTO {self.table} (id, contents) VALUES (?, ?)", params)
        self.db.commit()
        cur.close()


def create_sqlite_storage(path: str, table: str):
    return SqliteStorage(path, table)
